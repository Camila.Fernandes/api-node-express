## Node.js Authentication API

This is a simple Node.js authentication API providing user registration, login, and password recovery functionalities. The API uses Express.js framework to handle routes and bcrypt package for password hashing.

## Prerequisites

Node.js installed
npm (Node.js package manager)

## Installation
Clone this repository to your local environment:

    ```bash
        git clone https://github.com/your-user/api-authentication-node.git
    ```
Navigate to the application directory:

    ```bash
        cd api-authentication-node
    ```

Install project dependencies:
    ```bash
    npm install
    ```

Run the application:

    ```bash
    npm start
    ```

The application will run on port 3000 by default. You can change the port in the server.js file if needed.

## Endpoints

## Login
    ```bash
        POST /login
    ```
Endpoint to perform user login.

* Parameters:

email: User's email.
password: User's password.
* Responses:
    200: Login successful.
    401: Invalid password.
    404: User not found.
    500: Internal server error.

## Password Recovery
    ```bash
        POST /forgot-password
    ```
Endpoint to initiate the password recovery process.

* Parameters:
    email: User's email.

* Responses:
    200: Password recovery request processed successfully.
    500: Internal server error while processing the request.

## Contributing

Contributions are welcome! Feel free to open issues or send pull requests.

## License
This project is licensed under the MIT License.