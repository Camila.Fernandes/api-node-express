import express from 'express';
import bcrypt from 'bcrypt';
import bodyParser from 'body-parser';
import nodemailer from 'nodemailer';

const app = express();
const PORT = 4000;

const users = [];

const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'admin@gmail.com',
        pass: 'password'
    }
});

app.use(bodyParser.json());

/**
 * POST /login
 * 
 * Endpoint for user login.
 * 
 * @param {string} email - The email address of the user trying to login.
 * @param {string} password - The password of the user trying to login.
 * 
 * @returns {Object} - The response object indicating the status of the login request.
 * 
 * @throws {Error} - If an internal server error occurs.
 */
app.post('/login', async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = users.find(user => user.email === email);

        if (!user) {
            return res.status(404).send("User not found");
        }
        if (await bcrypt.compare(password, user.password)) {
            return res.status(200).send("Login successful");
        } else {
            return res.status(401).send("Invalid password");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

/**
 * POST /forgot-password
 * 
 * Endpoint for initiating the password recovery process.
 * 
 * @param {string} email - The email address associated with the user account.
 * 
 * @returns {Object} - The response object indicating the status of the password recovery request.
 * 
 * @throws {Error} - If an internal server error occurs.
 * 
 */
app.post('/forgot-password', async (req, res) => {
    try {
        const { email } = req.body;
        const user = users.find(user => user.email === email);

        if (user) {
            const { email } = req.body;        
            const mailOptions = {
                from: 'contact@gmail.com',
                to: email,
                subject: 'Password Recovery',
                text: 'Hello! You requested password recovery. Here is the link to reset your password: https://example.com/reset-password'
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.error(error);
                    res.status(500).send("Error processing password recovery request.");
                } else {
                    console.log('Email sent: ' + info.response);
                    res.status(200).send("Password recovery request processed successfully.");
                }
            });
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error while processing password recovery request.");
    }
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});